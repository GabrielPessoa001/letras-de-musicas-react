import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Header from './../Header/index';
import Dashboard from './../Dashboard/index';
import DetailsMusic from './../DetailsMusic/index';
import Footer from './../Footer/index';

export default function Routes() {
  return (
    <Router>
      <Header />
      <Route exact path="/" component={ Dashboard } />
      <Route exact path="/music" component={ DetailsMusic } />
      <Footer />
    </Router>
  );
}