import React, { Component } from 'react';

import './style.css';

class DetailsMusic extends Component {

  constructor(props) {
    super(props);

    this.state = {
      musica: "",
      artista: "",
      letra: "Carregando...",
      traducao: "Carregando..."
    }

    this.search = this.search.bind(this)
  }

  traducao_visivel() {
    let traducao_music = document.getElementsByClassName('traducao_music');
    let displaySetting = traducao_music[0].style.display;

    if (displaySetting === 'none') {
      traducao_music[0].style.display = 'inline';
    } else {
      traducao_music[0].style.display = 'none';
    }
  }

  search() {
    var url = require('url');
    var adr = window.location.href;
    var q = url.parse(adr, true);

    let url_vagalume = `https://api.vagalume.com.br/search.php?apikey=660a4395f992ff67786584e238f501aa${ q.search }&extra=relmus`;

    //alert(url_vagalume);

    fetch(url_vagalume)
    .then(res=>{
      return res.json()
    })
    .then(json=>{
      let letra = json.mus[0].text;
      let traducao = json.mus[0].text;

      console.log(letra);

      this.setState({ letra });
      this.setState({ traducao });
    })
    .catch(() => {
      let letra = "Música não encontrada";
      let traducao = "Music not found";

      this.setState({ letra });
      this.setState({ traducao });
    })
  }

  componentDidMount() {
    this.search();
    this.traducao_visivel();
  }

  exit() {
    window.location.href = `/`;
  }

  render() {

    return (
      <div className="container">

        <p className="text_music">{ this.state.letra }</p>
        <p className="traducao_music">{ this.state.traducao }</p>

        <input className="button_traducao" type="button" value="tradução" onClick={ this.traducao_visivel } />
        <input className="button_exit" href="/"  type="button" value="sair" onClick={ this.exit } />

      </div>
    )
  }
}

export default DetailsMusic;