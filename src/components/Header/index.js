import React, { Component } from 'react';

import './style.css';

class Header extends Component {
  render() {
    return (
      <div className="container_header">
        <h1 className="title_header">Music Search</h1>
      </div>
    )
  }
}

export default Header;