import React, { Component } from 'react';

import './style.css';

class Dashboard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      musica: "",
      artista: "",
      letra: "ALGUMA COISA",
      traducao: ""
    }

    this.search = this.search.bind(this)
  }

  search() {
    let art_music = `&art=${ this.state.artista }&mus=${ this.state.musica }`;

    window.location.href = `/music?${ art_music }`;
  }

  render() {
    return (
      <div className="container">
        <input className="label_search" type="text" placeholder="Música" onChange={ (event)=> { this.setState({ musica:event.target.value }) } }></input>
        <input className="label_search" type="text" placeholder="Artista" onChange={ (event)=> { this.setState({ artista:event.target.value }) } }></input>
        <input className="button_search" type="button" value="procurar" onClick={ this.search } />
      </div>
    )
  }
}

export default Dashboard;