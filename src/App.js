import React from 'react';

import GlobalStyle from './style/GlobalStyle';

import Routes from './components/Routes/index';

function App() {
  return (
    <>
      <Routes />

      <GlobalStyle />
    </>
  );
}

export default App;